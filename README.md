# Presto

Presto is a small Python script that helps creating [Reveal.js](http://lab.hakim.se/reveal-js/)-based HTML presentation 
from plain-text Markdown file. This script depends on [Mistune](https://github.com/lepture/mistune) library for markdown
rendering.

I found this approach to be pretty effective, as I'd to battle tested when preparing huge amount of workshop materials
in very tight time schedule. 

Recently I've found that there's support for markdown in Reveal.js done via third-party lib plugin. I'm still a bit more 
happier with this approach as:

- the syntax is way more compact (see next section) than embedding markdown into HTML sections,
- in contrast to Reveal.js approach the markdown gets compiled in "design-time" and saves some CPU power
and battery as there's no need to spend CPU cycles to translate markdown to HTML in browser runtime. (For price you need
to "recompile" your presentation each time you need up-to-date HTML. But that's something I as programmer am used to.


# Syntax

Basically the common markdown syntax (supported by mistune package) is extended by slide splitter syntax and simple 
parameter settings section in header. Let's take at simple example to get what I mean:

```text
title: hello world;
theme: blood;
extensions: highlight,zoom; 

----

# This is first slide

With some text in it.

----

# This is second slide

And some text here...

```

The four dashes surrounded by empty lines is the slide splitter. The first section is the section with presentation
parameters setting. Each parameter setting is in format  `<parameter name>: parameter value;` (note the trailing semicolon).

There are three main parameters you should set:

- `title`---is the HTML page title (shown in browser tab/window title),
- `theme`---is Reveal.js theme as defined in `css/theme/*.css` file
- `extensions`---is comma separated list of Reveal.js extensions to be turned on in generated presentation. 
Currently only `highlight` and `zoom` extensions are supported.

Other Reveal.js init parameters can be set here. See `hello.presto` for complete list.


## Sub-slides

There's also support for defining sub-slides which is a neat feature of Reveal.js. One can have multiple sub-slides 
within a slide sections, which are separated by four-tildas splitter. See following example to get a grasp.

```text
title: hello world;

----

# Hello from slide

~~~~

# First sub slide

~~~~

# Second sub slide

----

# New slide
```


# TODO

- Slide-specific parameters