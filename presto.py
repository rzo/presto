#!/usr/bin/env python3
import mistune
import re
import argparse
import shutil

from mistune import escape_link, escape


class Slide(object):
    def __init__(self, body="", attributes={}):
        self._sections = []
        if isinstance(body, str):
            self._sections.append(body)
        else:
            self._sections = list(body)
        self._attrs = attributes

    def single(self):
        return len(self._sections) == 1

    def sections(self):
        return self._sections

    def attributes(self):
        return self._attrs


class Presentation(object):
    def __init__(self, attrs={}):
        self._slides = []
        self._attrs = attrs

    def add_slide(self, slide):
        self._slides.append(slide)

    def slides(self):
        return self._slides

    def attributes(self):
        return self._attrs


class Parser(object):
    _rx_split = "\n" + r"--(\[([^\]]*)\])?--" + "\n"
    _rx_sub_sec_split = "\n" + r"~~(\[([^\]]*)\])?~~" + "\n"
    _rx_attr_cap = r"([A-Za-z0-9_:]*):([^;]*);"

    def __init__(self):
        pass

    def parse_args(self, buf):
        return {} if buf is None else {m.group(1).strip(): m.group(2).strip() for m in
                                       re.finditer(self._rx_attr_cap, buf)}

    def parse_section(self, idx, tokens):
        return tokens[idx + 2]

    def parse_slide_tokens(self, tokens, attrs):
        if len(tokens) == 1:
            return Slide(tokens[0], attrs)
        secs = [tokens[0]]
        for i in range(1, len(tokens), 3):
            secs.append(self.parse_section(i, tokens))
        return Slide(secs, attrs)

    def parse(self, buffer):
        l1_tokens = re.split(self._rx_split, buffer)
        ret = Presentation(self.parse_args(l1_tokens[0]))
        for i in range(1, len(l1_tokens), 3):
            attr_buf = l1_tokens[i + 1]
            attrs = self.parse_args(attr_buf)
            body = l1_tokens[i + 2]
            body_tokens = re.split(self._rx_sub_sec_split, body)
            ret.add_slide(self.parse_slide_tokens(body_tokens, attrs))

        return ret


template_main = """<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{title}</title>
        <link rel="stylesheet" href="fonts/fonts-all.css">
        <link rel="stylesheet" href="css/reveal.css">
        <link rel="stylesheet" href="css/columns.css">
        <link rel="stylesheet" href="css/theme/{theme}.css">
        <link rel="stylesheet" href="css/hljs-linenumbers.css">
        <link rel="stylesheet" href="lib/css/{hljsTheme}.css">
        <script src="lib/js/head.min.js"></script>
        <script src="lib/js/html5shiv.js"></script>
        <script src="lib/js/classList.js"></script>
    </head>
    <body>
        <div class="reveal">
            <div class="slides">
                {slidesSection}
            </div>
        </div>
        <script src="js/reveal.js"></script>
        <script>
            Reveal.initialize({initParams});
        </script>
    </body>
</html>"""

class MarkdownRenderer(mistune.Renderer):
    def __init__(self, link_prefix, **kwargs):
        super(MarkdownRenderer, self).__init__(**kwargs)
        self._link_prefix = link_prefix
        self._rx_src_style = re.compile(r'(.*\S)\s*\{([^}]*)\}')

    def _parse_src_style(self, src):
        style = None
        m = self._rx_src_style.match(src)
        if m:
            src = m.group(1)
            style = m.group(2)
        return src, style

    def _process_fancy_typo(self, text):
        fancyTypoSubs = [
            ('->', '&#8594;'),
            ('<-', '&#8592;'),
            ('=>', '&#8658;'),
            ('<=', '&#8656;'),
            ('---', '&mdash;'),
            ('--','&ndash;')]
        for src,rep in fancyTypoSubs:
            text = text.replace(src, rep)
        return text

    def image(self, src, title, text):
        src, style = self._parse_src_style(src)
        if not (src.startswith('/') or '://' in src):
            src = self._link_prefix + src
        src = escape_link(src)
        text = escape(text, quote=True)
        if title:
            title = escape(title, quote=True)
            html = '<img src="%s" alt="%s" title="%s"' % (src, text, title)
        else:
            html = '<img src="%s" alt="%s"' % (src, text)
        if style:
            html += f' style="{style}"'
        if self.options.get('use_xhtml'):
            return '%s />' % html
        return '%s>' % html

    def block_quote(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).block_quote(text)

    def link(self, link, title, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).link(link, title, text)

    def double_emphasis(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).double_emphasis(text)

    def paragraph(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).paragraph(text)

    def header(self, text, level, raw=None):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).header(text, level, raw)

    def list_item(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).list_item(text)

    def emphasis(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).emphasis(text)

    def text(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).text(text)

    def block_html(self, html):
        text = self._process_fancy_typo(html)
        return super(MarkdownRenderer, self).block_html(html)

    def footnote_item(self, key, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).footnote_item(key, text)

    def strikethrough(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).strikethrough(text)

    def footnotes(self, text):
        text = self._process_fancy_typo(text)
        return super(MarkdownRenderer, self).footnotes(text)


class Renderer(object):
    _initParamNames = 'controls progress slideNumber history keyboard overview center touch loop rtl shuffle ' \
                      'fragments embedded help showNotes autoSlide autoSlideStoppable autoSlideMethod mouseWheel ' \
                      'hideAddressBar previewLinks transition transitionSpeed backgroundTransition viewDistance ' \
                      'parallaxBackgroundImage parallaxBackgroundSize parallaxBackgroundHorizontal ' \
                      'parallaxBackgroundVertical width height margin minScale maxScale'.split()

    def __init__(self, theme="white", hljsTheme="idea"):
        self._theme = theme
        self._hljsTheme = hljsTheme
        self._markdown = mistune.Markdown(renderer=MarkdownRenderer("../"))

    def markdown(self, text):
        return self._markdown.render(text)

    def render_section(self, section):
        columns = section.split("\n....\n")
        if len(columns) == 1:
            sectionHtml = self.markdown(section)
        else:
            sectionHtml = self.markdown(columns[0])
            sectionHtml += '<div class="cols-container">' \
                           + "\n".join(('<div class="two-cols">\n%s\n</div>' % self.markdown(c) for c in columns[1:-1])) \
                           + "</div>\n"
            sectionHtml += self.markdown(columns[-1])
        return "<section>{slideHtml}</section>\n".format(slideHtml=sectionHtml)

    def render_slide(self, slide):
        secs = slide.sections()
        if slide.single():
            return self.render_section(secs[0])
        return "<section>\n" + "\n".join((self.render_section(s) for s in secs)) + "</section>\n"

    def render(self, presentation, defaultTitle=""):
        attrs = presentation.attributes()
        theme = attrs["theme"] if "theme" in attrs else self._theme
        hljsTheme = attrs["hljsTheme "] if "hljsTheme " in attrs else self._hljsTheme
        title = attrs["title"] if "title" in attrs else defaultTitle
        extensions = [x.strip() for x in attrs["extensions"].split(",")] if "extensions" in attrs else []

        slidesSec = "\n".join((self.render_slide(sl) for sl in presentation.slides()))
        initParams = ""
        deps = []
        for ext in extensions:
            if ext == "zoom":
                deps.append(
                    "{ src: 'plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } }")
            elif ext == "highlight":
                deps.append(
                    "{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); "
                    "hljs.initLineNumbersOnLoad(); } }")
            elif ext in ('notes', 'speakernotes'):
                deps.extend([
                    "{ src: 'plugin/notes/notes.js', async: true }",
                    "{ src: 'socket.io/socket.io.js', async: true }",
                    "{ src: 'plugin/notes-server/client.js', async: true }"
                ])
            else:
                print("Unknown extension %s" % ext)
        for attrName in attrs:
            if attrName in self._initParamNames:
                if len(initParams) > 0:
                    initParams += ",\n  "
                initParams += "%s:%s" % (attrName, attrs[attrName])

        if len(deps) > 0:
            if len(initParams) > 0:
                initParams += ",\n  "
            initParams += "dependencies:[%s]" % (",\n    ".join(deps))

        if len(initParams) > 0:
            initParams = "{\n%s\n}" % initParams

        return template_main.format(slidesSection=slidesSec, theme=theme, hljsTheme=hljsTheme, initParams=initParams, title=title)


def bootstrap_slide(fn, title="hello world"):
    buf = """title: {title};
theme: blood;
extensions: highlight,zoom;
hljsTheme: idea;


// Display controls in the bottom right corner
controls: true;

// Display a presentation progress bar
progress: true;

// Display the page number of the current slide
slideNumber: true;

// Push each slide change to the browser history
history: false;

// Enable keyboard shortcuts for navigation
keyboard: true;

// Enable the slide overview mode
overview: true;

// Vertical centering of slides
center: true;

// Enables touch navigation on devices with touch input
touch: true;

// Loop the presentation
loop: false;

// Change the presentation direction to be RTL
rtl: false;

// Randomizes the order of slides each time the presentation loads
shuffle: false;

// Turns fragments on and off globally
fragments: true;

// Flags if the presentation is running in an embedded mode;
// i.e. contained within a limited portion of the screen
embedded: false;

// Flags if we should show a help overlay when the questionmark
// key is pressed
help: true;

// Flags if speaker notes should be visible to all viewers
showNotes: false;

// Number of milliseconds between automatically proceeding to the
// next slide; disabled when set to 0; this value can be overwritten
// by using a data-autoslide attribute on your slides
autoSlide: 0;

// Stop auto-sliding after user input
autoSlideStoppable: true;

// Use this method for navigation when auto-sliding
autoSlideMethod: Reveal.navigateNext;

// Enable slide navigation via mouse wheel
mouseWheel: false;

// Hides the address bar on mobile devices
hideAddressBar: true;

// Opens links in an iframe preview overlay
previewLinks: false;

// Transition style
transition: 'default'; // none/fade/slide/convex/concave/zoom

// Transition speed
transitionSpeed: 'default'; // default/fast/slow

// Transition style for full page slide backgrounds
backgroundTransition: 'default'; // none/fade/slide/convex/concave/zoom

// Number of slides away from the current that are visible
viewDistance: 3;

// Parallax background image
parallaxBackgroundImage: ''; // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"

// Parallax background size
parallaxBackgroundSize: ''; // CSS syntax; e.g. "2100px 900px"

// Number of pixels to move the parallax background per slide
// - Calculated automatically unless specified
// - Set to 0 to disable movement along an axis
parallaxBackgroundHorizontal: null;
parallaxBackgroundVertical: null;

----

# {title}
"""
    with open(fn, "wt") as fp:
        fp.write(buf.format(title=title))


def prepare_html_dir(dirPath):
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

    srcDir = os.path.dirname(os.path.realpath(__file__))
    dirsToCopy = ["css", "fonts", "lib", "plugin","js"]
    for d in dirsToCopy:
        src=os.path.join(srcDir, d)
        dst=os.path.join(dirPath, d)
        if os.path.exists(dst):
            shutil.rmtree(dst)
        shutil.copytree(src, dst)


def run_init(args):
    prepare_html_dir("html")


def run_new(args):
    title=args.title
    if title is None:
        title,_ = os.path.splitext(args.slide)
    bootstrap_slide(args.slide, title=title)


def run_build(args):
    build_files(args.slide)


def build_files(files):
    parser = Parser()
    rdr = Renderer()
    for presto_fn in files:
        with open(presto_fn, "rt") as fp:
            presentation = parser.parse(fp.read())
        base_fn, ext = os.path.splitext(presto_fn)
        dir_path = os.path.dirname(base_fn)
        base_fn = os.path.basename(base_fn)
        fn = os.path.join(dir_path, "html", base_fn + ".html")
        with open(fn, "wt") as fp:
            fp.write(rdr.render(presentation, base_fn))


if __name__ == "__main__":
    import sys, os.path

    ap = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]))
    sps = ap.add_subparsers(dest='cmd', required=True)
    apInit = sps.add_parser("init")
    apInit.set_defaults(func=run_init)
    apNew = sps.add_parser("new")
    apNew.add_argument("--title")
    apNew.set_defaults(func=run_new, title=None)
    apNew.add_argument("slide")
    apBuild = sps.add_parser("build")
    apBuild.add_argument("slide", nargs="+")
    apBuild.set_defaults(func=run_build)

    args = ap.parse_args()
    args.func(args)

