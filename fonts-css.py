#!/usr/bin/env python
import os
baseDir=os.path.dirname(__file__)
fontsDir=os.path.join(baseDir, "fonts")

template="""
@font-face {{
    font-family: "{family}";
    src: url("{fontPath}");
    {fontSpec}
}}
"""

fonts={}

fontWeights={"Regular": "400", "Medium": "500", "Bold": "700", "Black": "900",
             "Light": "300", "Thin": "200", "ExtraLight": "100", "Hairline": "100",
             "Italic": "400"}
def findWeight(style):
    for weight in fontWeights:
        if style.startswith(weight):
            return fontWeights[weight]
    return None
for root, dirs, files in os.walk(fontsDir):
    for f in files:
        if f.lower().endswith(".ttf"):
            fileTokens = f[:-4].split('-')
            font = fileTokens[0]
            style = fileTokens[1]
            if font not in fonts:
                fonts[font] = {}
            fonts[font][style] = f
for font in fonts:
    for style in fonts[font]:
        italic = style.endswith("Italic")
        weight = findWeight(style)
        spec = "font-weight: %s; " % weight
        if italic:
            spec += "font-style: italic; "
        fontPath="fonts/%s-%s.ttf" % (font, style)
        buf=template.format(family=font, fontPath=fontPath, fontSpec=spec)
        print buf